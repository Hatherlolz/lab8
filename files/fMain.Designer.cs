﻿namespace WindowsFormsApp2
{
    partial class fMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbLochInfo = new System.Windows.Forms.TextBox();
            this.btnNameLoch = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbLochInfo
            // 
            this.tbLochInfo.Location = new System.Drawing.Point(12, 12);
            this.tbLochInfo.Multiline = true;
            this.tbLochInfo.Name = "tbLochInfo";
            this.tbLochInfo.ReadOnly = true;
            this.tbLochInfo.Size = new System.Drawing.Size(404, 162);
            this.tbLochInfo.TabIndex = 0;
            this.tbLochInfo.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // btnNameLoch
            // 
            this.btnNameLoch.Location = new System.Drawing.Point(422, 24);
            this.btnNameLoch.Name = "btnNameLoch";
            this.btnNameLoch.Size = new System.Drawing.Size(112, 23);
            this.btnNameLoch.TabIndex = 1;
            this.btnNameLoch.Text = "Додати озеро";
            this.btnNameLoch.UseVisualStyleBackColor = true;
            this.btnNameLoch.Click += new System.EventHandler(this.BtnNameLoch_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(422, 125);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(112, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Закрити";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 186);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnNameLoch);
            this.Controls.Add(this.tbLochInfo);
            this.MaximizeBox = false;
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Лабараторна робота №8";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbLochInfo;
        private System.Windows.Forms.Button btnNameLoch;
        private System.Windows.Forms.Button btnClose;
    }
}

