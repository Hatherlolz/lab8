﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void BtnNameLoch_Click(object sender, EventArgs e)
        {
            Loch loch = new Loch();
            fLoch fl = new fLoch(loch);

            if (fl.ShowDialog() == DialogResult.OK)
            {
                tbLochInfo.Text += 
                    string.Format("{0}, {1}, {2}. Площа: {3:0.000} кв. км. Обсяг води: {4:0.000}м.куб. Колір води: {5}, {6}", loch.Name, loch.Country, loch.Region, loch.Area,
                    loch.WaterVolume, loch.Color, loch.HasWaterFall ? "Є водоспад" : "Немає водоспада");
                loch.CleanlinessOfTheLoch();
            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Припинити роботу застосунку?", "Припинити роботу", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit();
        }
    }
}
