﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class fLoch : Form
    {
        public Loch TheLoch;
        public fLoch(Loch t)
        {
            TheLoch = t;
            InitializeComponent();
        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Label3_Click(object sender, EventArgs e)
        {

        }

        private void Label7_Click(object sender, EventArgs e)
        {

        }

        private void BtnOk_Click(object sender, EventArgs e)
        {
            TheLoch.Name = tbName.Text.Trim();
            TheLoch.Country = tbCountry.Text.Trim();
            TheLoch.Region = tbRegion.Text.Trim();
            TheLoch.Area = double.Parse(tbArea.Text.Trim());
            TheLoch.WaterVolume = double.Parse(tbWaterVolume.Text.Trim());
            TheLoch.Color = tbColor.Text.Trim();
            TheLoch.HasWaterFall = chHasWaterFall.Checked;

            DialogResult = DialogResult.OK;


        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void FLoch_Load(object sender, EventArgs e)
        {
            if (TheLoch != null)
            {
                tbName.Text = TheLoch.Name;
                tbCountry.Text = TheLoch.Country;
                tbRegion.Text = TheLoch.Region;
                tbArea.Text = TheLoch.Area.ToString();
                tbWaterVolume.Text = TheLoch.WaterVolume.ToString();
                tbColor.Text = TheLoch.Color;
                chHasWaterFall.Checked = TheLoch.HasWaterFall;
            }
        }
    }
}
