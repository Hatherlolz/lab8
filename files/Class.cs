﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2
{
    public class Loch
    {
        public string Name;
        public string Country;
        public string Region;
        public double Area;
        public double WaterVolume;
        public string Color;
        public bool HasWaterFall;

        public void CleanlinessOfTheLoch()
        {
            if (Country == "Україна" || Country == "Росія" || Country == "Білорусь")
            {
                Console.WriteLine("Озеро брудне");
            }
            else
                Console.Write("Озеро Чисте");

        }
    }
}
